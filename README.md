# API Consumer Correios

API responsável por consumir informações dos Correios.

### Settings:
- [Lumen Framework - Versão 8.2.3](https://lumen.laravel.com/docs/8.x)
- [PHP - Versão 7.3](https://www.php.net/downloads.php)

### Requirements:
- [Lumen Framework](https://lumen.laravel.com/docs/8.x/installation#server-requirements)

### Endpoint API Consumer Correios.

- http://localhost
- http://localhost/v1/healthy
- http://localhost/v1/search/{cep}


### Installation and Configuration:

- 1) Fazer o clone projeto
        
       `git clone https://gitlab.com/LeomarDuarte/api-consumer-correios.git`


- 2) Acessar o diretório api-consumer-correios e executar os seguintes comandos e/ou configurações:
        
        configurar o arquivo .env
        Instalar dependências do projeto com: `composer install`

- 3) Subir a aplicação:
        
        `php -S localhost:8000 -t public`

